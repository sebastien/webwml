#use wml::debian::cdimage title="Δικτυακή εγκατάσταση από ένα μίνιμαλ CD"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="40eeef5c10b97819abf8467d7f2a8038a8729ff8" maintainer="galaxico"

<p>Ένα CD <q>διακτυακής εγκατάστασης</q> ή <q>netinst</q> είναι ένα μοναδικό CD που σας επιτρέπει να εγκαταστήσετε ολόκληρο το λειτουργικό σύστημα. Αυτό το μοναδικό CD περιέχει μόνο την ελάχιστη ποσότητα λογισμικού για να ξεκινήσετε την εγκατάσταση και να φέρετε τα υπόλοιπα πακέτα από το Διαδίκτυο.</p>

<p><strong>Τι είναι καλλίτερο για μένα &mdash; το μίνιμαλ εκκινήσιμο CD-ROM ή τα πλήρη CD;</strong> Εξαρτάται, αλλά νομίζουμε ότι σε πολλές περιπτώσεις το η εικόνα του μίνιμαλ CD είναι καλλίτερη &mdash; πρώτα απ' όλα, μεταφορτώνετε τα πακέτα που επιλέξατε για εγκατάσταση στο μηχάνημά σας, που σας γλιτώνει και χρόνο και δεδομένα στο διαδίκτυο. Από την άλλη, τα πλήρη CD είναι πιο κατάλληλα όταν κάνετε εγκατάσταση σε περισσότερα από ένα μηχανήματα, ή σε μηχανήματα χωρίς μια δωρεάν σύνδεση στο Διαδίκτυο.</p>

<p><strong>What types of network connections are supported
during installation?</strong>
The network install assumes that you have a connection to the
Internet. Various different ways are supported for this, like
analogue PPP dial-up, Ethernet, WLAN (with some restrictions), but 
ISDN is not &mdash; sorry!</p>

<p>The following minimal bootable CD images are available for
download:</p>

<ul>

  <li>Official <q>netinst</q> images for the <q>stable</q> release &mdash; <a
  href="#netinst-stable">see below</a></li>

  <li>Images for the <q>testing</q> release, both daily builds and known
  working snapshots, see the <a
  href="$(DEVEL)/debian-installer/">Debian-Installer page</a>.</li>

</ul>


<h2 id="netinst-stable">Official netinst images for the <q>stable</q> release</h2>

<p>Up to 300&nbsp;MB in size, this image contains the installer and a
small set of packages which allows the installation of a (very) basic
system.</p>

<div class="line">
<div class="item col50">
<p><strong>netinst CD image (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>netinst CD image (generally 150-300 MB, varies by architecture)</strong></p>
	  <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>For information what these files are and how to use them, please see
the <a href="../faq/">FAQ</a>.</p>

<p>Once you have downloaded the images, be sure to have a look at the
<a href="$(HOME)/releases/stable/installmanual">detailed
information about the installation process</a>.</p>
