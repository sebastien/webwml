<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21775">CVE-2021-21775</a>

    <p>Marcin Towalski discovered that a specially crafted web page can
    lead to a potential information leak and further memory
    corruption. In order to trigger the vulnerability, a victim must
    be tricked into visiting a malicious webpage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21779">CVE-2021-21779</a>

    <p>Marcin Towalski discovered that a specially crafted web page can
    lead to a potential information leak and further memory
    corruption. In order to trigger the vulnerability, a victim must
    be tricked into visiting a malicious webpage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30663">CVE-2021-30663</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30665">CVE-2021-30665</a>

    <p>yangkang discovered that processing maliciously crafted web
    content may lead to arbitrary code execution. Apple is aware of a
    report that this issue may have been actively exploited.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30689">CVE-2021-30689</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to universal cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30720">CVE-2021-30720</a>

    <p>David Schutz discovered that a malicious website may be able to
    access restricted ports on arbitrary servers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30734">CVE-2021-30734</a>

    <p>Jack Dates discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30744">CVE-2021-30744</a>

    <p>Dan Hite discovered that processing maliciously crafted web
    content may lead to universal cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30749">CVE-2021-30749</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30758">CVE-2021-30758</a>

    <p>Christoph Guttandin discovered that processing maliciously crafted
    web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30795">CVE-2021-30795</a>

    <p>Sergei Glazunov discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30797">CVE-2021-30797</a>

    <p>Ivan Fratric discovered that processing maliciously crafted web
    content may lead to code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30799">CVE-2021-30799</a>

    <p>Sergei Glazunov discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.32.3-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4945.data"
# $Id: $
