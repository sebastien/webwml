#use wml::debian::translation-check translation="0be6ccc9d7c1aaf4b17f2ba90ede309d57601910" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OTRS, le
système au code source ouvert de requêtes par tickets, qui pourraient conduire
à la divulgation d'informations ou une élévation des privilèges. De nouvelles
options de configuration ont été ajoutées pour résoudre ces problèmes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>

<p>Un attaquant pourrait envoyer un courriel malveillant à un système OTRS. Si
un utilisateur agent connecté le cite, le courriel pourrait faire que le
navigateur charge des ressources d’images externes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12497">CVE-2019-12497</a>

<p>Dans le frontal de l’utilisateur ou externe, les informations personnelles
des agents peuvent être divulguées telles que le nom et l’adresse de courriel
dans des notes externes.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1+deb8u10.</p>
<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1816.data"
# $Id: $
