#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers vulnérabilités conduisant à un déni de service ou d’autres possibles
impacts non précisés ont été découverts dans sam2p, un utilitaire pour convertir
des images matricielles en EPS, PDF et autres formats.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12578">CVE-2018-12578</a>

<p>Dépassement de tampon basé sur le tas dans bmp_compress1_row. Merci à Peter
Szabo pour la fourniture du correctif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12601">CVE-2018-12601</a>

<p>Dépassement de tampon basé sur le tas dans la fonction ReadImage, dans le
fichier input-tga.ci. Merci à Peter Szabo pour la fourniture du correctif.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.49.2-3+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets sam2p.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1463.data"
# $Id: $
