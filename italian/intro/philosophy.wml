#use wml::debian::template title="La nostra filosofia: perché e come lo facciamo"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">COSA è Debian?</a></li>
<li><a href="#free">È completamente libera?</a></li>
<li><a href="#how">Come la comunità lavora sul progetto?</a>
<li><a href="#history">Come è cominciato tutto?</a>
</ul>

<h2><a name="what">COSA è Debian?</a></h2>

<p>Il <a href="$(HOME)/">Progetto Debian</a> è una associazione di persone
che ha come scopo comune la creazione di un sistema operativo <a href="free">
libero</a>. Il sistema operativo che abbiamo creato si chiama <strong>Debian</strong>.
</p>

<p>Un sistema operativo è l'insieme dei programmi di base e altri vari
strumenti che permettono al computer di funzionare.
Al centro di un sistema operativo c'è il kernel. Il kernel è il
programma fondamentale sul computer ed esegue tutte le
operazioni di base e permette di eseguire programmi.
</p>

<p>I sistemi Debian attuali utilizzano il kernel 
<a href="https://www.kernel.org/">Linux</a> o quello
<a href="https://www.freebsd.org/">FreeBSD</a>. Linux è un software scritto
inizialmente da <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus
Torvalds</a> e che è supportato da migliaia di programmatori nel mondo.
FreeBSD è un sistema operativo che comprende un kernel e vari applicativi.
</p>

<p>Inoltre, stiamo lavorando per fornire Debian su altri kernel, principalmente
per <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
Hurd è un insieme di serventi che si basano su un microkernel
(come ad esempio Mach) e che implementano vari servizi. Hurd è software
libero prodotto dal <a href="https://www.gnu.org/">progetto GNU</a>.
</p>

<p>Larga parte degli applicativi di base che completano il sistema operativo
provengono da <a href="https://www.gnu.org/">GNU</a>; da questo si hanno i nomi
GNU/Linux, GNU/kFreeBSD e GNU/Hurd. Anche questi applicativi sono software
libero.
</p>

<p>Certo, la cosa che la gente vuole è il software applicativo. Strumenti che
permettano loro di ottenere che sia fatto ciò che vogliono, dallo
scrivere documenti a gestire il lavoro ad eseguire giochi a scrivere altro software.
Debian si presenta con più di <packages_in_stable>
<a href="$(DISTRIB)/packages">pacchetti</a>
(software precompilato messo insieme in un formato
adatto ad essere installato in maniera semplice sulla propria macchina),
un gestore di pacchetti (APT) e altri programmi di utilità che permettono
di gestire migliaia di pacchetti su migliaia di computer con la stessa
semplicità con cui si installa un'applicazione. Tutto completamente
<a href="free">libero</a>.
</p>

<p>È un po' come una torre. Alla base c'è il kernel. Sopra ci sono gli
strumenti di base. Poi viene tutto il software che funziona sul computer.
Alla sommità c'è Debian &mdash; che organizza e dispone tutto
in maniera che tutto funzioni all'unisono.
</p>


<h2>È completamente <a href="free" name="free">libera?</a></h2>

<p>Quando usiamo la parola <q>libero</q>, ci riferiamo alla
<strong>libertà</strong> del software. Potete leggere di più su cosa
<a href="free">intendiamo con <q>software libero</q></a> e 
<a href="https://www.gnu.org/philosophy/free-sw">cosa scrive la Free
Software Foundation</a> sull'argomento.
</p>

<p>Ci si può chiedere: perché la gente dovrebbe spendere ore del proprio
tempo a scrivere software, a metterlo insieme attentamente, e poi
<em>darlo</em> tutto via?
Le risposte sono varie quanto le persone che contribuiscono.
Ad alcune persone piace aiutare gli altri.
Molte scrivono programmi per imparare di più sui computer.
Sempre più gente cerca vie per evitare il prezzo gonfiato dei software.
Una moltitudine in crescita contribuisce come ringraziamento per tutto il
software che hanno ricevuto.
Nelle università molti creano software libero per ottenere i risultati
delle loro ricerche in un utilizzo più ampio.
Compagnie commerciali aiutano a mantenere il software libero per avere
voce su come procede lo sviluppo:
non c'è maniera più rapida di ottenere una nuova caratteristica che
implementarla da solo!
Di sicuro, molti di noi lo trovano semplicemente un grosso divertimento.
</p>
<p>Debian è talmente dedita al software libero che abbiamo pensato fosse
utile formalizzare questa dedizione in un documento di qualche genere. Per
questo motivo nacque il nostro <a href="$(HOME)/social_contract">Contratto
Sociale</a>.
</p>

<p>Anche se Debian crede nel software libero, ci sono casi in cui la
gente vuole o ha bisogno di mettere software non libero nella propria
macchina.
Debian supporterà ciò ogni volta che sarà possibile.
Ci sono anche un numero crescente di pacchetti il cui solo utilizzo è
l'installazione di software non libero su un sistema Debian.
</p>

<h2><a name="how">Come la comunità lavora sul progetto?</a></h2>

<p>Debian è prodotta da almeno un migliaio di
sviluppatori da tutto il
<a href="$(DEVEL)/developers.loc">mondo</a> che si
offrono volontari nel loro tempo libero.
Pochi si sono davvero incontrati di persona.
La comunicazione è effettuata principalmente attraverso posta
elettronica (liste di messaggi su lists.debian.org) e IRC (il canale #debian
su irc.debian.org).
</p>

<p>Il Progetto Debian ha un'articolata <a href="organization">struttura
interna</a>. Per maggiori informazioni su come Debian sia strutturata si
guardi la sezione <a href="$(DEVEL)/">l'angolo degli sviluppatori</a>.</p>

<p>
I principali documenti in cui è descritto come lavora la comunità sono:
</p>

<ul>
<li><a href="$(DEVEL)/constitution">Costituzione Debian</a></li>
<li><a href="../social_contract">Contratto Sociale Debian e le Linee Guida per il Software Libero</a></li>
<li><a href="diversity">Dichiarazione sulla diversità</a></li>
<li><a href="../code_of_conduct">Codice di condotta</a></li>
<li><a href="../doc/developers-reference/">Guida di riferimento per sviluppatori</a></li>
<li><a href="../doc/debian-policy/">Policy Debian</a></li>
</ul>


<h2><a name="history">Come è cominciato tutto?</a></h2>

<p>Debian nacque nell'agosto 1993 da Ian Murdock come una nuova distribuzione
che potesse finalmente essere veramente aperta, nello spirito Linux e GNU.
Debian fu ideata per essere poi definita in maniera coscienziosa e mantenuta
con molta cura. È stata molto ristretta all'inizio, formata solo da uno
stretto gruppo di hacker del software libero; gradualmente è poi
cresciuta fino a diventare una grande e organizzata comunità di
sviluppatori e utenti. Vedi <a href="$(DOC)/manuals/project-history/">la
storia in dettaglio</a>.
</p>

<p>Visto che molta gente lo ha chiesto, Debian si pronuncia /&#712;de.bi.&#601;n/.
Deriva dal nome del suo ideatore, Ian Murdock, e di sua moglie, Debra.
</p>
