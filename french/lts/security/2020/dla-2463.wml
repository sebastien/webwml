#use wml::debian::translation-check translation="81bb16a1bc882871b51a2e86a7002ab1eec224a8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichiers SMB/CIFS, d'impression et de connexion pour Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1472">CVE-2020-1472</a>

<p>Contrôleur de domaine non authentifié compromis par un contournement du
chiffrement de Netlogon. Cette vulnérabilité inclut les versions ZeroLogon et
non ZeroLogon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10704">CVE-2020-10704</a>

<p>Un utilisateur non autorisé peut déclencher un déni de service à l'aide d'un
débordement de pile dans le serveur AD DC LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

<p>Déréférencement de pointeur NULL et utilisation de mémoire après libération
dans le serveur AD DC LDAP avec ASQ, VLV et paged_results.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10745">CVE-2020-10745</a>

<p>Déni de service suite à l’exploitation de compression de réponse à NetBIOS
lors d’une réponse de résolution de nom à travers TCP/IP et des paquets DNS,
provoquant une charge excessive de CPU pour le service AD DC de Samba.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10760">CVE-2020-10760</a>

<p>L’utilisation de paged_results ou de contrôles VLV avec le serveur LDAP
Global Catalog dans le service AD DC peut causer une utilisation de mémoire
après libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14303">CVE-2020-14303</a>

<p>Déni de service suite à une attente du CPU et à son incapacité à traiter les
requêtes suivantes une fois que le serveur AD DC NBT a reçu un paquet UDP
vide (longueur zéro) sur le port 137.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14318">CVE-2020-14318</a>

<p>Vérification manquante des permissions de gestionnaire dans ChangeNotify</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14323">CVE-2020-14323</a>

<p>Utilisateur non privilégié pouvant planter winbind à l’aide de lookupsids
DOS non valables</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14383">CVE-2020-14383</a>

<p>Plantage du serveur DNS à l’aide d’enregistrements non valables suite à une
non-initialisation de variables.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:4.5.16+dfsg-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">https://security-tracker.debian.org/tracker/samba</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2463.data"
# $Id: $
