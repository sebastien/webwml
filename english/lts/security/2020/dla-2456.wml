<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

    <p>In Lib/tarfile.py, an attacker is able to craft a TAR
    archive leading to an infinite loop when opened by tarfile.open,
    because _proc_pax lacks header validation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a>

    <p>http.client allows CRLF injection if the attacker controls
    the HTTP request method</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.3-1+deb9u3.</p>

<p>We recommend that you upgrade your python3.5 packages.</p>

<p>For the detailed security status of python3.5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2456.data"
# $Id: $
