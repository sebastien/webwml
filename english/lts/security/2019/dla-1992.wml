<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Manfred Paul and Lukas Schauer reported that the .charkeys procedure in
Ghostscript, the GPL PostScript/PDF interpreter, does not properly
restrict privileged calls, which could result in bypass of file system
restrictions of the dSAFER sandbox.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.26a~dfsg-0+deb8u6.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1992.data"
# $Id: $
