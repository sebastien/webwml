#use wml::debian::blend title="라이브 이미지 다운로드"
#use wml::debian::blends::gis
#use "../navbar.inc"
#use wml::debian::translation-check translation="a2a93c6af14d9de0d3c3aa2b2d7fa4d06a48ee43" maintainer="Sebul"

<p>데비안 GIS 퓨어 블렌드는 데비안 GIS 퓨어 블렌드를 설치하지 않고 시도해 볼 수 있는 <b>라이브 DVD 이미지</b>를 만듭니다.
이미지는 설치관리자도 포함해서 블렌드로부터 온 패키지와 함께 설치할 수 있습니다.</p>

<h2>데비안 GIS 블렌드 <em>안정</em></h2>

<p>데비안 GIS 블렌드 라이브 DVD의 미리보기 릴리스를 다운로드 할 수 있습니다.</p>

<p>최근 안정 릴리스: <strong><stable-version/></strong>.</p>

<ul>
        <li><a href="<stable-amd64-url/>">amd64 라이브 DVD 이미지 (ISO)</a> 
        <li><a href="<stable-i386-url/>">i386 라이브 DVD 이미지 (ISO)</a> 
        <li><a href="<stable-source-url/>">라이브 DVD 이미지 소스 아카이브 (tar)</a> 
</ul>

<p>웹부트 이미지, 체크섬 및 GPG 서명은 <a
href="https://cdimage.debian.org/cdimage/blends-live/">전체 파일 목록</a>을 참조하세요.</p>

<h2>데비안 GIS 블렌드 <em>testing</em></h2>

<p>가까운 미래에, 현재는 가능하지 않지만 라이브 DVD는 스트레치(현재 데비안 테스팅 배포)를 위해 빌드될 겁니다.
</p>

<h2>시작하기</h2>

<h3>DVD 사용하기</h3>

<p>대부분의 최신 운영 체제에는 ISO 이미지를 DVD 미디어로 굽는 규정이 있습니다.
데비안 CD FAQ는 <a href="https://www.debian.org/CD/faq/index#record-unix">리눅스</a>, <a
href="https://www.debian.org/CD/faq/index#record-windows">윈도</a> 및 <a
href="https://www.debian.org/CD/faq/index#record-mac">맥 OS</a>를
사용하여 ISO 이미지를 굽는 방법에 대한 지침을 제공합니다.
어려움이 있으면 웹 검색 엔진을 사용하면 필요한 답변을 얻을 겁니다.
</p>

<h3>USB 메모리 사용하기</h3>

<p>ISO 이미지는 하이브리드 이미지로 만들어지므로, unetbootin 같은 특별한 소프트웨어를 쓰지 않고 직접 USB에 복사할 수 있습니다.
리눅스 시스템에서 다음과 같이 할 수 있습니다:</p>

<pre>sudo dd if=/path/to/debian-gis-live-image.iso of=/dev/sd<b>X</b></pre>

<p>dmesg 명령의 출력이 USB 메모리의 장치명을 알려줄 겁니다. 여기서 <b>X</b>는 주어진 문자로 바꾸어야 할 겁니다.</p>

