#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des chercheurs en sécurité ont identifié une exécution spéculative de
méthodes par canal auxiliaire ayant le potentiel de récolter des données
sensibles à partir de périphériques informatiques avec des processeurs
et systèmes d’exploitation différents.</p>

<p>Cette mise à jour nécessite celle du paquet intel-microcode qui n’est pas
libre. C’est en relation avec DLA-1446-1 et ajoute plus de mitigations pour
des types de processeurs supplémentaires d’Intel.</p>

<p>Pour plus d’informations, lire aussi les alertes de sécurité officielles
d’Intel :</p>

<ul>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00088.html"></li>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00115.html"></li>
<li><url "https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00161.html"></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.20180807a.1~deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1506.data"
# $Id: $
