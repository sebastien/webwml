#use wml::debian::translation-check translation="7909e175532fe8bfb8cbc8f55539498e926e67a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans libxml2, la bibliothèque XML de GNOME.
Il est appelé attaque <q>Parameter Laughs</q> et concerne l’expansion de
paramètres d’entités. Il est similaire à l’attaque <q>Billion Laughs</q> trouvée
précédemment dans libexpat. Plus d’information est disponible dans [1].</p>

<p>[1] <a href="https://blog.hartwork.org/posts/cve-2021-3541-parameter-laughs-fixed-in-libxml2-2-9-11/">https://blog.hartwork.org/posts/cve-2021-3541-parameter-laughs-fixed-in-libxml2-2-9-11/</a>.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.9.4+dfsg1-2.2+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2669.data"
# $Id: $
