<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in SPICE before version 0.14.1 where the
generated code used for demarshalling messages lacked sufficient bounds
checks. A malicious client or server, after authentication, could send
specially crafted messages to its peer which would result in a crash or,
potentially, other impacts.</p>

<p>The issue has been fixed by upstream by bailing out with an error if the
pointer to the start of some message data is strictly greater than the
pointer to the end of the  message data.</p>

<p>The above issue and fix have already been announced for the <q>spice</q>
Debian package (as DLA-1486-1 [1]). This announcement is about the
"spice-gtk" Debian package (which ships some copies of code from the
<q>spice</q> package, where the fix of this issue had to be applied).</p>

<p>[1] <a href="https://lists.debian.org/debian-lts-announce/2018/08/msg00037.html">https://lists.debian.org/debian-lts-announce/2018/08/msg00037.html</a></p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.25-1+deb8u1.</p>

<p>We recommend that you upgrade your spice-gtk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1489.data"
# $Id: $
