<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that logback, a flexible logging library for Java,
would deserialize data from untrusted sockets which may lead to the
execution of arbitrary code. This issue has been resolved by adding a
whitelist to use only trusted classes.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.0.4-1+deb7u1.</p>

<p>We recommend that you upgrade your logback packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-888.data"
# $Id: $
