<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an integer overflow vulnerability in librabbitmq, a library for robust messaging between applications and servers.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18609">CVE-2019-18609</a>

    <p>An issue was discovered in amqp_handle_input in amqp_connection.c in rabbitmq-c 0.9.0. There is an integer overflow that leads to heap memory corruption in the handling of CONNECTION_STATE_HEADER. A rogue server could return a malicious frame header that leads to a smaller target_size value than needed. This condition is then carried on to a memcpy function that copies too much data into a heap buffer.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.5.2-2+deb8u1.</p>

<p>We recommend that you upgrade your librabbitmq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2022.data"
# $Id: $
