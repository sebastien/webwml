#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f"
<define-tag pagetitle>Ampere dona hardware servidor Arm64 a Debian para fortalecer el ecosistema Arm</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
<a href="https://amperecomputing.com/">Ampere®</a> se ha asociado con Debian
para dar soporte a nuestra infraestructura hardware mediante la donación
de tres servidores Arm64 de alto rendimiento de Ampere.
Estos servidores Lenovo ThinkSystem HR330A contienen una CPU eMAG de Ampere con un
procesador Arm®v8 de 64 bits específicamente diseñado para servidores de la nube,
256GB de RAM, 960GB de SSD dual y una tarjeta de red de 25GbE y doble puerto.
</p>

<p>
Los servidores donados se han desplegado en la Universidad de Columbia Británica,
nuestro socio para alojamiento de servicios en Vancouver, Canadá.
El personal administrador del sistema Debian (DSA) los han configurado para ejecutar
daemons de compilación arm64/armhf/armel, en sustitución de daemons de compilación que se ejecutaban en tarjetas
de nivel de desarrollo («developer-grade»), menos potentes. En máquinas virtuales con la mitad de 
vCPU asignadas, el resultado ha sido que el tiempo para construir paquetes Arm* se
ha reducido a la mital con el sistema eMAG de Ampere. Otro beneficio de este generoso regalo
es que permitirá al personal DSA migrar algunos servicios generales de Debian que operan en
nuestra infraestructura actual y proveer de máquinas virtuales a otros
equipos de Debian (por ejemplo: Integración continua, Garantía de calidad, etc.)
que necesitan acceso a la arquitectura Arm64. 
</p>

<p>
<q>Nuestra colaboración con Debian apoya la estrategia de nuestros desarrolladores de ampliar
las comunidades de código abierto cuyo software corre en servidores Ampere para desarrollar más
el ecosistema Arm64 y habilitar la creación de nuevas aplicaciones</q>,
dijo Mauri Whalen, vicepresidenta de ingeniería de software en Ampere.
<q>Debian es una comunidad bien gestionada y respetada, y estamos orgullosos de trabajar con ellos.</q>
</p>

<p>
<q>Los administradores del sistema Debian están agradecidos a Ampere por la donación de
servidores Arm64 de alto nivel («carrier-grade»). Tener servidores con interfaces de gestión estándar
integradas, como la interfaz de gestión inteligente de plataformas (IPMI, por sus siglas en inglés),
y con la garantía hardware y soporte de Lenovo tras ellos,
es precisamente lo que los DSA han estado deseando para la arquitectura Arm64.
Estos servidores son muy potentes y están muy bien equipados: pensamos utilizarlos
para servicios generales además de usarlos para los daemons de compilación Arm64. Creo que resultarán
ser muy interesantes para operadores de la nube y estoy muy contento de que Ampere Computing
se haya asociado con Debian</q> - Luca Filipozzi, administrador del sistema Debian.
</p>

<p>
Solo a través de la donación de trabajo voluntario, equipamiento y servicios
en especie y financiación es capaz Debian de cumplir su compromiso de ser un
sistema operativo libre. Estamos muy agradecidos por la generosidad de Ampere.
</p>

<h2>Acerca de Ampere Computing</h2>
<p>
Ampere está diseñando el futuro de la nube de hiperescala y de la computación cercana («edge computing») con el
primer procesador nativo de la nube del mundo. Construido para la nube con una arquitectura moderna
basada en servidores Arm de 64 bits, Ampere da a sus clientes la libertad de acelerar
la entrega de todas las aplicaciones de computación en la nube. Con un rendimiento líder
en la industria de la nube, eficiencia en el consumo de potencia y escalabilidad, los procesadores Ampere se adaptan
al continuo crecimiento de la nube y de la computación cercana.
</p>

<h2>Acerca de Debian</h2>
<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>


<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;press@debian.org&gt;.</p>
