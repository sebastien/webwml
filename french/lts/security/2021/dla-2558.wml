#use wml::debian::translation-check translation="12640a377aa22d9e9af7f7369983db016862e485" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>xterm jusqu’à Patch n° 365 permet à des attaquants distants de provoquer un
déni de service (erreur de segmentation) ou, éventuellement, d’avoir un impact
non précisé à l'aide d'une séquence de caractères UTF-8 contrefaite.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 327-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xterm.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xterm, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xterm">\
https://security-tracker.debian.org/tracker/xterm</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2558.data"
# $Id: $
