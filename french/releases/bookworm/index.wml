#use wml::debian::template title="Informations sur la version «&nbsp;Bookworm&nbsp;» de Debian"
#use wml::debian::translation-check translation="43ed34ae31e2b37b42af378b2dcc661486ce5646" maintainer="Jean-Pierre Giraud"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"

# Translators:
# cf. ../<other_release>/index.html

<if-stable-release release="bookworm">

<p>
La version&nbsp;<current_release_bookworm> de Debian (connue sous le nom
de <em>Bookworm</em>) a été publiée le <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "La version 12.0 a été initialement publiée le <:=spokendate('XXXXXXXX'):>."
/>
Cette version
comprend de nombreuses modifications décrites dans notre <a
href="$(HOME)/News/XXXX/XXXXXXXX">communiqué de presse</a> et les <a
href="releasenotes">notes de publication</a>.
</p>

#<p><strong>Debian 12 a été remplacée par
#<a href="../trixie/">Debian 13 (<q>Trixie</q>)</a>.
#Les mises à jour de sécurité sont arrêtées depuis le <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Néanmoins, Bookworm bénéficie de la prise en charge à long terme
#(<q>Long Term Support</q> – LTS) jusqu'à la fin du mois de xxxxx 20xx.
#Cette prise en charge est limitée aux architectures i386, amd64, armel,
#armhf et arm64. Toutes les autres architectures ne sont plus prises en
#charge dans Bookworm.
#Pour de plus amples informations, veuillez consulter la
#<a href="https://wiki.debian.org/fr/LTS">section dédiée à LTS du wiki Debian</a>.
#</strong></p>

<p>
Pour obtenir et installer Debian, veuillez vous reporter à la page
des <a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à partir d'une ancienne
version de Debian, veuillez vous reporter aux instructions des <a
href="releasenotes">notes de publication</a>.
</p>

### Activate the following when LTS period starts.
#<p>Architectures prises en charge durant la prise en charge à long terme :</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>
Les architectures suivantes sont gérées par la version initiale de Bookworm :
</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="reportingbugs">signaler d'autres problèmes</a>.
</p>

<p>
Enfin, nous avons une liste de <a href="credits">personnes à remercier</a> pour
leur participation à cette publication.
</p>

</if-stable-release>

<if-stable-release release="bullseye">

<p>
Le nom de code de la prochaine version majeure de Debian après <a
href="../bullseye/">Bullseye</a> est <q>Bookworm</q>.
</p>

<p>
Cette version a été initialisée à partir d'une copie de Bullseye, et se trouve
pour l'instant dans une phase dite <q><a
href="$(DOC)/manuals/debian-faq/ftparchives#testing">de test</a></q>. Cela signifie
que vous ne devriez pas souffrir des mêmes problèmes qu'avec les distributions
instable ou expérimentale, car les paquets n'entrent dans cette distribution
qu'après une certaine période de test, et s'ils n'ont pas de bogues critiques.
</p>

<p>
Veuillez noter que les mises à jour relatives à la sécurité pour la
distribution de test <strong>ne</strong> sont <strong>pas</strong> encore gérées par
l'équipe en charge de la sécurité. En conséquence de quoi, ces mises à jour ne
sont soumises à <strong>aucune</strong> contrainte de temps.
# Pour plus
# d'informations, veuillez consulter l'<a
# href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">annonce</a>
# de l'équipe en charge de la sécurité de la distribution de test.
Vous êtes encouragé à changer vos entrées dans votre fichier sources.list de testing
à <code>bullseye</code> pour le moment si vous avez besoin de la gestion de la sécurité.
Consultez également la <a href="$(HOME)/security/faq#testing">\
FAQ de l'équipe en charge de la sécurité</a> pour la distribution de test.
</p>

<p>
Un <a href="releasenotes">brouillon des
notes de publications</a> peut être disponible.
Veuillez également prendre connaissance des <a
href="https://bugs.debian.org/release-notes">\
propositions d'ajouts aux notes de publication</a>.
</p>

<p>
Si vous souhaitez des images d'installation et de la documentation sur
l'installation de la distribution de test, veuillez vous reporter
à <a href="$(HOME)/devel/debian-installer/">la page de l'installateur Debian</a>.
</p>

<p>
Pour savoir comment fonctionne la distribution de test, lisez ces <a
href="$(HOME)/devel/testing">informations pour les développeurs</a>.
</p>

<p>
Une question récurrente est de savoir s'il existe un indicateur sur l'état du
processus de création de cette version. Malheureusement, il n'y en a pas, mais
voici des liens qui indiquent les problèmes à régler avant que la distribution
ne puisse voir le jour&nbsp;:
</p>

<ul>
  <li><a href="https://release.debian.org/">Page générique d'état des
      publications</a>&nbsp;;</li>
  <li><a href="https://bugs.debian.org/release-critical/">Bogues
      critiques</a>&nbsp;;</li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bogues du système de
      base</a>&nbsp;;</li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bogues dans les
      paquets de priorité standard et appartenant à des
      métapaquets</a>.</li>
</ul>

<p>
D'autre part, des comptes-rendus sont postés sur la <a
href="https://lists.debian.org/debian-devel-announce/">liste de diffusion
debian-devel-announce</a> par le responsable de la publication.
</p>

</if-stable-release>

<if-stable-release release="buster">

<p>Le nom de code de la prochaine version majeure de Debian après
<a href="../bullseye /">Bullseye</a> est <q>Bookworm</q>. Actuellement,
<q>Bullseye</q> n'a pas encore été publiée. Aussi, <q>Bookworm</q>
est encore loin.</p>

</if-stable-release>
