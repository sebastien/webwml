<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two more security issues have been corrected in the libav multimedia library. This is a follow-up announcement for DLA-1611-1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6823">CVE-2015-6823</a>

    <p>The allocate_buffers function in libavcodec/alac.c did not initialize
    certain context data, which allowed remote attackers to cause a
    denial of service (segmentation violation) or possibly have
    unspecified other impact via crafted Apple Lossless Audio Codec
    (ALAC) data. This issues has now been addressed by clearing pointers
    in avcodec/alac.c's allocate_buffers().</p>

    <p>Other than stated in debian/changelog of upload 6:11.12-1~deb8u2,
    this issue only now got fixed with upload of 6:11.12-1~deb8u3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6824">CVE-2015-6824</a>

    <p>The sws_init_context function in libswscale/utils.c did not
    initialize certain pixbuf data structures, which allowed remote
    attackers to cause a denial of service (segmentation violation) or
    possibly have unspecified other impact via crafted video data. In
    swscale/utils.c now these pix buffers get cleared which fixes use of
    uninitialized memory.</p>

    <p>Other than stated in debian/changelog of upload 6:11.12-1~deb8u2,
    this issue only now got fixed with upload of 6:11.12-1~deb8u3.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u3.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1611-2.data"
# $Id: $
