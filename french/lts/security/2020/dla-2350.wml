#use wml::debian::translation-check translation="a75ba607468cfe0a5654eb463c3570a389c465e4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans le logiciel de groupe de travail Horde, il existait une vulnérabilité de
script intersite (XSS) à l’aide du champ Name lors de la création d’une nouvelle
Resource. Cela pourrait avoir été exploité pour l’exécution de code à distance
après la compromission d’un compte d’administrateur, à cause de la possibilité
de contournement du mécanisme de protection CSRF 
(<a href="https://security-tracker.debian.org/tracker/CVE-2015-7984">CVE-2015-7984</a>).</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 4.2.19-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-horde-kronolith.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php-horde-kronolith, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php-horde-kronolith">https://security-tracker.debian.org/tracker/php-horde-kronolith</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2350.data"
# $Id: $
