#use wml::debian::template title="Debian 11 &mdash; Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="4ca2253325130f7e96bf2644d31cf5a95fdf7bcc" maintainer="Luca Monducci"
#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemi conosciuti</toc-add-entry>

<toc-add-entry name="security">Problemi legati alla sicurezza</toc-add-entry>

<p>Il team di sicurezza Debian distribuisce aggiornamenti per i pacchetti
della distribuzione stable nei quali si trovano problemi legati alla
sicurezza. Si consultino le <a href="$(HOME)/security/">pagine della
sicurezza</a> per avere informazioni su eventuali problemi legati alla
sicurezza di <q>bullseye</q>.</p>

<p>Chi utilizza APT può aggiungere la seguente riga a
<tt>/etc/apt/sources.list</tt> per avere accesso agli aggiornamenti
legati alla sicurezza:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Dopodiché eseguire <kbd>apt update</kbd> seguito da
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Rilasci minori (Point releases)</toc-add-entry>

<p>A volte, in caso di parecchi problemi critici o aggiornamenti della
sicurezza, la distribuzione viene aggiornata. Di norma questi sono chiamati
rilasci minori.</p>

<!-- <ul>
<li>Il primo rilascio minore, 11.1, è stato rilasciato
il <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_buster> 11.0 "

<p>Non ci sono ancora rilasci minori per Debian 11.</p>" "

<p>Si veda il <a href=http://archive.debian.org/debian/dists/bullseye/ChangeLog>\
ChangeLog</a> per dettagli sui cambiamenti tra 11.0 e
<current_release_bullseye/>.</p>" />

<p>Gli aggiornamenti per una distribuzione stabile attraversano un lungo
periodo di verifica prima di essere accettati nell'archivio. Nonostante
ciò essi sono disponibili nella directory
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> di qualsiasi mirror dell'archivio
Debian.</p>

<p>Se si utilizza APT per aggiornare i propri pacchetti, si possono
installare gli aggiornamenti proposti aggiungendo la seguente riga in
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 11 point release
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Dopodiché si esegua <kbd>apt update</kbd> seguito da
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema d'installazione</toc-add-entry>

<p>Per informazioni sugli errori e gli aggiornamenti per il sistema
d'installazione si veda la pagina con le <a href="debian-installer">\
informazioni sull'installazione</a>.</p>
