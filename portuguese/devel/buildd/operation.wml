#use wml::debian::template title="Esboço da operação da rede autobuilder" BARETITLE="true"
#use wml::debian::translation-check translation="4094875c3a7aafc988b5f218b0a629fab415552a"

<P>
No coração do sistema está o banco de dados <TT>wanna-build</TT>, o qual
monitora as versões e estados dos pacotes. O <TT>quinn-diff</TT>
compara as listas de pacotes para dada arquitetura contra a lista de
pacotes-fonte após cada atualização de repositório, e alimenta o banco de
dados com a lista de pacotes que precisam de recompilação, momento em que
eles entram no estado <TT>Needs-Build</TT>.

<P>
Todo daemon de construção (pode haver mais de um) consulta o banco de dados
regularmente por tais pacotes e pega alguns deles de modo que entrem
no estado <TT>Building</TT>. Claro, pessoas também podem
pegar os pacotes, por exemplo, nos casos especiais em que a compilação
automática não é possível. Aqui também vemos o segundo propósito de
<TT>wanna-build</TT>: ele assegura que a mesma versão do pacote não será
construída duas vezes.

<DIV class="center"><A name="autobuilder34"></A>
<IMG SRC="scheme.png" alt="Autobuilder scheme">
<p><STRONG>Figura:</STRONG>
Estados do pacote e transições</p>
</DIV>

<P>
Se tudo correr bem, um pacote finalizado pode ser enviado posteriormente,
que é outro estado, <TT>Uploaded</TT>. A seguir, ele eventualmente será
instalado no repositório do Debian para assim aparecer na lista de pacotes
atualizados para uma determinada arquitetura. Essa lista será integrada ao
banco de dados, de modo que o pacote vá para o estado <TT>Installed</TT>
e permaneça ali até a próxima versão do pacote-fonte.

<P>
Há muitos outros estados; eles incluem: <TT>Failed</TT> é para
pacotes que falharam na construção devido a erros nos pacotes-fonte, e
espera-se que esses erros sejam corrigidos em uma próxima versão (após
o problema ser relatado, claro). Então, uma nova versão entrará diretamente
em <TT>Needs-Build</TT>, mas com um alerta de que alguma coisa estava
errada com a versão anterior. Junto com esse estado, uma descrição de
erro é armazenada. O estado <TT>Dep-Wait</TT> é usado
quando um pacote precisa de outros pacotes para ser compilado, mas estes
ainda não estão disponíveis e precisam ser construídos anteriormente. Este
estado armazena uma lista de pacotes requeridos e talvez as versões; e se
todos eles são reconhecidos como instalados, o estado muda de volta para
<TT>Needs-Build</TT>.

<P>
Como já vimos, o daemon de construção pega os pacotes do
banco de dados para compilá-los. Vamos examinar melhor: se ele tem alguns
pacotes para construir, ele usa o <TT>sbuild</TT> para o processo de
compilação real, e para cada construção um log é enviado por e-mail ao(à)
mantenedor(a) do daemon. Ele(a) revisa o log e decide o que fazer
com o pacote: fazer upload, defini-lo como <TT>Failed</TT> ou
<TT>Dep-Wait</TT> e tentar novamente, etc. Se uma confirmação positiva
(um arquivo assinado <TT>.changes</TT>) é recebida, o daemon move o
pacote para um diretório de upload, de onde todos os pacotes são enviados
por um cron job.

<P>
Examinar os arquivos de log é a única intervenção humana em todo o
processo se nenhum erro acontece. Existem duas boas razões para não
automatizarmos ainda mais: primeiro, algumas vezes as construções terminam
com um resultado "OK", mas mesmo assim a construção falhou por motivos que
são invisíveis à máquina. E segundo, o upload direto demandaria assinaturas
de PGP automáticas nos arquivos resultantes com uma chave sem senha na
máquina de construção. Eu considero isso uma falha de segurança inaceitável.

<P>
O script de construção <TT>sbuild</TT> meio que somente chama algumas
ferramentas padrões do Debian para compilar os fontes. Ele também ajuda
com algumas tarefas comuns e contabilidades, e com a instalação
automática de build-dependencies como requisitadas pelo pacote que está sendo
construído.

<hrline />
<p><small>Artigo desenvolvido por Roman Hodek para o
6th International Linux-Kongress 1999; ele foi levemente atualizado
para melhor refletir a realidade atual por Philipp Kern em 2009</small></p>
