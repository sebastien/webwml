<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several minor issues have been fixed in tomcat8, a Java Servlet and
JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5388">CVE-2016-5388</a>

    <p>Apache Tomcat, when the CGI Servlet is enabled, follows RFC 3875
    section 4.1.18 and therefore does not protect applications from
    the presence of untrusted client data in the HTTP_PROXY
    environment variable, which might allow remote attackers to
    redirect an application's outbound HTTP traffic to an arbitrary
    proxy server via a crafted Proxy header in an HTTP request, aka an
    <q>httpoxy</q> issue.  The <q>cgi</q> servlet now has a <q>envHttpHeaders</q>
    parameter to filter environment variables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8014">CVE-2018-8014</a>

    <p>The defaults settings for the CORS filter provided in Apache
    Tomcat are insecure and enable <q>supportsCredentials</q> for all
    origins. It is expected that users of the CORS filter will have
    configured it appropriately for their environment rather than
    using it in the default configuration. Therefore, it is expected
    that most users will not be impacted by this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0221">CVE-2019-0221</a>

    <p>The SSI printenv command in Apache Tomcat echoes user provided
    data without escaping and is, therefore, vulnerable to XSS. SSI is
    disabled by default. The printenv command is intended for
    debugging and is unlikely to be present in a production website.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8.0.14-1+deb8u15.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1883.data"
# $Id: $
