<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>debian-security-support, the Debian security support coverage checker, has
been updated in stretch-security.</p>

<p>This marks the end of life of the mongodb package in stretch due to
licence incompatibility.</p>

<p>See <a href="https://lists.debian.org/debian-lts/2020/11/msg00058.html">https://lists.debian.org/debian-lts/2020/11/msg00058.html</a>
and <a href="https://bugs.debian.org/915537">https://bugs.debian.org/915537</a>
for more information.</p>

<p>For Debian 9 <q>stretch</q>, this problem has been documented in version
1:9+2020.12.04 of the debian-security-support package.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2482.data"
# $Id: $
