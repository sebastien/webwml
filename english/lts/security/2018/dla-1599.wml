<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were found in QEMU, a fast processor emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2391">CVE-2016-2391</a>

    <p>Zuozhi Fzz discovered that eof_times in USB OHCI emulation support
    could be used to cause a denial of service, via a null pointer
    dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2392">CVE-2016-2392</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-2538">CVE-2016-2538</a>

    <p>Qinghao Tang found a NULL pointer dereference and multiple integer
    overflows in the USB Net device support that could allow local guest
    OS administrators to cause a denial of service. These issues related
    to remote NDIS control message handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2841">CVE-2016-2841</a>

    <p>Yang Hongke reported an infinite loop vulnerability in the NE2000 NIC
    emulation support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2857">CVE-2016-2857</a>

    <p>Liu Ling found a flaw in QEMU IP checksum routines. Attackers could
    take advantage of this issue to cause QEMU to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2858">CVE-2016-2858</a>

    <p>Arbitrary stack based allocation in the Pseudo Random Number Generator
    (PRNG) back-end support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4001">CVE-2016-4001</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-4002">CVE-2016-4002</a>

    <p>Oleksandr Bazhaniuk reported buffer overflows in the Stellaris and the
    MIPSnet ethernet controllers emulation. Remote malicious users could
    use these issues to cause QEMU to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4020">CVE-2016-4020</a>

    <p>Donghai Zdh reported that QEMU incorrectly handled the access to the
    Task Priority Register (TPR), allowing local guest OS administrators
    to obtain sensitive information from host stack memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4037">CVE-2016-4037</a>

    <p>Du Shaobo found an infinite loop vulnerability in the USB EHCI
    emulation support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4439">CVE-2016-4439</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-4441">CVE-2016-4441</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5238">CVE-2016-5238</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5338">CVE-2016-5338</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-6351">CVE-2016-6351</a>

    <p>Li Qiang found different issues in the QEMU 53C9X Fast SCSI Controller
    (FSC) emulation support, that made it possible for local guest OS
    privileged users to cause denials of service or potentially execute
    arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4453">CVE-2016-4453</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-4454">CVE-2016-4454</a>

    <p>Li Qiang reported issues in the QEMU VMWare VGA module handling, that
    may be used to cause QEMU to crash, or to obtain host sensitive
    information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4952">CVE-2016-4952</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-7421">CVE-2016-7421</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-7156">CVE-2016-7156</a>

    <p>Li Qiang reported flaws in the VMware paravirtual SCSI bus emulation
    support. These issues concern an out-of-bounds access and infinite
    loops, that allowed local guest OS privileged users to cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5105">CVE-2016-5105</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5106">CVE-2016-5106</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5107">CVE-2016-5107</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5337">CVE-2016-5337</a>

    <p>Li Qiang discovered several issues in the MegaRAID SAS 8708EM2 Host
    Bus Adapter emulation support. These issues include stack information
    leakage while reading configuration and out-of-bounds write and read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6834">CVE-2016-6834</a>

    <p>Li Qiang reported an infinite loop vulnerability during packet
    fragmentation in the network transport abstraction layer support.
    Local guest OS privileged users could made use of this flaw to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6836">CVE-2016-6836</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-6888">CVE-2016-6888</a>

    <p>Li Qiang found issues in the VMWare VMXNET3 network card emulation
    support, relating to information leak and integer overflow in packet
    initialisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7116">CVE-2016-7116</a>

    <p>Felix Wilhel discovered a directory traversal flaw in the Plan 9 File
    System (9pfs), exploitable by local guest OS privileged users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7155">CVE-2016-7155</a>

    <p>Tom Victor and Li Qiang reported an out-of-bounds read and an infinite
    loop in the VMware paravirtual SCSI bus emulation support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7161">CVE-2016-7161</a>

    <p>Hu Chaojian reported a heap overflow in the xlnx.xps-ethernetlite
    emulation support. Privileged users in local guest OS could made use
    of this to cause QEMU to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7170">CVE-2016-7170</a>

    <p>Qinghao Tang and Li Qiang reported a flaw in the QEMU VMWare VGA
    module, that could be used by privileged user in local guest OS to
    cause QEMU to crash via an out-of-bounds stack memory access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7908">CVE-2016-7908</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-7909">CVE-2016-7909</a>

    <p>Li Qiang reported infinite loop vulnerabilities in the ColdFire Fast
    Ethernet Controller and the AMD PC-Net II (Am79C970A) emulations.
    These flaws allowed local guest OS administrators to cause a denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8909">CVE-2016-8909</a>

    <p>Huawei PSIRT found an infinite loop vulnerability in the Intel HDA
    emulation support, relating to DMA buffer stream processing.
    Privileged users in local guest OS could made use of this to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8910">CVE-2016-8910</a>

    <p>Andrew Henderson reported an infinite loop in the RTL8139 ethernet
    controller emulation support. Privileged users inside a local guest OS
    could made use of this to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9101">CVE-2016-9101</a>

    <p>Li Qiang reported a memory leakage in the i8255x (PRO100) ethernet
    controller emulation support.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9102">CVE-2016-9102</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9103">CVE-2016-9103</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9104">CVE-2016-9104</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9105">CVE-2016-9105</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9106">CVE-2016-9106</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-8577">CVE-2016-8577</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-8578">CVE-2016-8578</a>

    <p>Li Qiang reported various Plan 9 File System (9pfs) security issues,
    including host memory leakage and denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10664">CVE-2017-10664</a>

    <p>Denial of service in the qemu-nbd (QEMU Disk Network Block Device)
    Server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10839">CVE-2018-10839</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-17962">CVE-2018-17962</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-17963">CVE-2018-17963</a>

    <p>Daniel Shapira reported several integer overflows in the packet
    handling in ethernet controllers emulated by QEMU. These issues could
    lead to denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u8.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1599.data"
# $Id: $
