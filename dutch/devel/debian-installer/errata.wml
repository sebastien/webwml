#use wml::debian::template title="Het installatieprogramma van Debian - errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="548e836a27bec221897b3d647cb4f4f338c565a4"

<h1>Errata voor <humanversion /></h1>

<p>
Dit is een lijst van gekende problemen in de <humanversion />-release van het
installatieprogramma van Debian. Indien u uw probleem hierin niet vermeld
vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installatierapport</a>
waarin u het probleem beschrijft.
</p>

<dl class="gloss">
     <dt>Defecte noodmodus in het grafisch installatieprogramma</dt>
     <dd>Tijdens het testen van het image van RC 1 van Bullseye ontdekte
     men dat de noodmodus niet blijkt te werken
     (<a href="https://bugs.debian.org/987377">#987377</a>).
     Daarenboven moet het label “Rescue” in de banner van het grafisch
     ontwerp voor Bullseye aangepast worden.
     <br />
     <b>Toestand:</b> Opgelost in Bullseye RC 2.</dd>

     <dt>Voor veel grafische kaarten van AMD is amdgpu-firmware vereist</dt>
     <dd>Er blijkt een toenemende behoefte te zijn om amdgpu-firmware te
     installeren (via het niet-vrije pakket <code>firmware-amd-graphics</code>)
     om te vermijden dat het scherm zwart blijft bij het opstarten van
     het geïnstalleerde systeem. Zelfs bij gebruik van een installatie-image
     dat alle firmwarepakketten bevat, detecteert het installatieprogramma
     vanaf RC 1 van Bullseye niet de behoefte aan dat specifieke onderdeel. Zie
     het <a href="https://bugs.debian.org/989863">overkoepelende bugrapport</a>
     om onze inspanningen te volgen.
     <br />
     <b>Toestand:</b> Opgelost in Bullseye RC 3.</dd>

     <dt>Installaties van een grafische werkomgeving kunnen mislukken wanneer
     men enkel met cd#1 installeert</dt>
     <dd>Door de beperkte ruimte op de eerste cd, passen niet alle verwachte
      GNOME-desktoppakketten op cd#1. Voor een succesvolle installatie moet u
      extra pakketbronnen (bijv. een tweede cd of een
      netwerkspiegelserver) of eerder een dvd gebruiken. <br />
      <b>Toestand:</b> Het is onwaarschijnlijk dat met meer inspanningen meer
      pakketten in te passen zijn op cd#1. </dd>

     <dt>LUKS2 is niet compatibel met GRUB's cryptodisk-ondersteuning</dt>
     <dd>Pas onlangs werd vastgesteld dat GRUB geen ondersteuning biedt voor
      LUKS2, Dit betekent dat gebruikers die <tt>GRUB_ENABLE_CRYPTODISK</tt>
      willen gebruiken en een aparte niet-geëncrypteerde <tt>/boot</tt> willen
      vermijden, dit niet zullen kunnen doen
      (<a href="https://bugs.debian.org/927165">#927165</a>). Deze opstelling
      wordt sowieso toch niet ondersteund in het installatieprogramma, maar
      het zou logisch zijn om deze beperking op zijn minst prominenter te
      documenteren en om op zijn minst de mogelijkheid te hebben om voor LUKS1
      te kiezen tijdens het installatieproces.
     <br />
     <b>Toestand:</b> Er zijn enkele ideeën geuit over de bug; de ontwikkelaars
      van cryptsetup schreven wat
      <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">specifieke documentatie</a>.</dd>

</dl>
