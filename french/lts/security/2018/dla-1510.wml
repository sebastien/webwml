#use wml::debian::translation-check translation="e03db691eef0bf1e048da79524a1cbc851b57faf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans GlusterFS, un
système de fichiers réparti. Des problèmes de dépassement de tampon et traversée
de répertoires pourraient conduire à une divulgation d'informations, un déni de
service ou l'exécution de code arbitraire.</p>

<p>Pour résoudre ces vulnérabilités de sécurité les limitations suivantes ont
été faites dans GlusterFS :</p>

<ul>
<li>l’ouverture, la lecture et l’écriture de fichiers spéciaux tels que
 caractère ou bloc ne sont plus permis ;</li>
<li>io-stat xlator peut vider l’information de statistique uniquement pour le
 répertoire /run/gluster.</li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.5.2-2+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets glusterfs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1510.data"
# $Id: $
