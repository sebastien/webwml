#use wml::debian::template title="Παροράματα του Εγκαταστάτη του Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5513b0df9f9525c15c9a757a14ac534e8d3ac03e" maintainer="galaxico"

<h1>Παροράματα για την έκδοση <humanversion /></h1>

<p>
Αυτή είναι μια λίστα γνωστών προβλημάτων στην έκδοση <humanversion /> του Εγκαταστάτη του Debian. Αν δεν βρείτε το πρόβλημα που αντιμετωπίσατε να αναγράφεται εδώ, παρακαλούμε στείλτε μας μια
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">αναφορά εγκατάστασης</a> περιγράφοντας το πρόβλημα.
</p>

<dl class="gloss">
     <dt>Το GNOME ίσως να αποτυγχάνει να ξεκινήσει με μερικές ρυθμίσεις εικονικών μηχανών</dt>
     <dd>Παρατηρήθηκε στη διάρκεια δοκιμών της εικόνας Stretch Alpha 4 ότι το
     GNOME ίσως να αποτυγχάνει να αρχίσει ανάλογα με τις ρυθμίσεις που χρησιμοποιούνται για εικονικές μηχανές. Φαίνεται ότι χρησιμοποιώντας το cirrus ως προσομοίωση τσιπ εικόνας είναι μια χαρά.
     <br />
     <b>Κατάσταση:</b> Υπό διερεύνηση.</dd>

     <dt>Εγκαταστάσεις της επιφάνειας εργασίας πιθανόν να μην δουλεύουν χρησιμοποιώντας μόνο το CD#1</dt>
     <dd>Εξαιτίας περιορισμών χώρου στο πρώτο CD, δεν χωράνε όλα τα αναμενόμενα πακέτα του GNOME στο CD#1. Για μια επιτυχή εγκατάσταση, χρησιμοποιήστε επιπλέον πηγές (πχ. ένα δεύτερο CD ή ένα διαδικτυακό καθρέφτη) ή διαφορετικά χρησιμοποιήστε ένα DVD. <br /> <b>Κατάσταση:</b> Δεν ενδέχεται να καταβληθούν περισσότερες προσπάθειες για να χωρέσουν περισσότερα πακέτα στο 
     CD#1. </dd>

     <dt>Το θέμα που χρησιμοποιείται στον εγκαταστάτη</dt>
     <dd>Δεν υπάρχει ακόμα καλλιτεχνική δουλειά για την έκδοση Buster, και ο εγκαταστάτης χρησιμοποιεί ακόμα το θέμα της Stretch.
     <br />
     <b>Κατάσταση:</b>
       Διορθώθηκε στην έκδοση Buster RC 1: το θέμα <a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">futurePrototype</a> έχει ενσωματωθεί.</dd>

     <dt>Προβληματικές συντμήσεις πληκτολογίου στον διορθωτή κειμένου nano</dt>
     <dd>Η έκδοση του nano που χρησιμοποιείται στον Εγκαταστάτη του Debian δεν παρουσιάζει τις ίδιες συντμήσεις με το συνηθισμένο πακέτο του nano (στα εγκατεστημένα συστήματα). Πιο συγκεκριμένα η υπόδειξη χρήσης της σύντμησης 
     <tt>C-s</tt> για την <q>Αποθήκευση</q> φαίνεται να είναι πρόβλημα για άτομα που χρησιμοποιούν τον εγκαταστάτη σε μια σειριακή κονσόλα
       (<a href="https://bugs.debian.org/915017">#915017</a>).
     <br />
     <b>Κατάσταση:</b> Διορθώθηκε στην έκδοση Buster RC 1.</dd>

     <dt>Η υποστήριξη του TLS στο wget δεν δουλεύει</dt>
     <dd>Μια αλλαγή στην βιβιοθήκη του openssl οδηγεί σε μερικά ζητήματα για τους χρήστες της, καθώς ένα αρχείο ρύθμισης που αρχικά ήταν προαιρετικό ξαφνικά έγινε υποχρεωτικό. Ως συνέπεια, τουλάχιστον το <tt>wget</tt> δεν υποστηρίζει το πρωτόκολλο HTTPS στον εγκαταστάτη
       (<a href="https://bugs.debian.org/926315">#926315</a>).
     <br />
     <b>Κατάσταση:</b> Διορθώθηκε στην έκδοση Buster RC 2.</dd>

     <dt>Ζητήματα σχετικά με την εντροπία</dt>
     <dd>Ακόμα και αν η υποστήριξη του HTTPS στο wget είναι λειτουργική, μπορεί κανείς να βρεθεί αντιμέτωπος με προβλήματα σχετιζόμενα με την εντροπία του συστήματος ανάλογα με την διαθεσιμότητα μιας γεννήτρια τυχαίων αριθμών στο υλικό (RNG) και/ή έναν επαρκή αριθμό συμβάντων για την τροφοδότηση της δεξαμενής εντροπίας
       (<a href="https://bugs.debian.org/923675">#923675</a>). Συνήθη συμπτώματα μπορεί να είναι μια μακρά καθυστέρηση στην πρώτη σύνδεση HTTPS  στη διάρκεια της δημιουργίας ενός SSH ζεύγους κλειδιών (keypair).
     <br />
     <b>Κατάσταση:</b> Διορθώθηκε στην έκδοση Buster RC 2.</dd>

     <dt>Το LUKS2 είναι μη συμβατό με την υποστήριξη του κρυπτοδίσκου από το GRUB</dt>
     <dd>Ανακαλύφθηκε μόνο αργότερα ότι το GRUB δεν έχει υποστήριξη για το
       LUKS2. Αυτό σημαίνει ότι χρήστες που θέλουν να χρησιμοποιήσουν την επιλογή
       <tt>GRUB_ENABLE_CRYPTODISK</tt> και να αποφύγουν μια ξεχωριστή,
       μη κρυπτογραφημένη κατάτμηση <tt>/boot</tt>, δεν θα μπορούν να το κάνουν.
       (<a href="https://bugs.debian.org/927165">#927165</a>). Αυτή η ρύθμιση δεν υποστηρίζεται από τον εγκαταστάτη έτσι κι αλλιώς, αλλά θα είχε νόημα τουλάχιστον να τεκμηριωθούν αυτοί οι περιορισμοί εμφανέστερα, και υπάρχει τουλάχιστον η δυνατότητα να επιλέξει κανείς στη διάρκεια της διαδικασίας εγκατάστασης το LUKS1.
     <br />
     <b>Κατάσταση:</b> Μερικές ιδέες έχουν εκφραστεί για το σφάλμα αυτό· οι συντηρητές του cryptsetup έχουν γράψει κάποια <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html"> συγκεκριμένη τεκμηρίωση</a>.</dd>

     <dt>Αποτυχία προρρύθμισης για προσαρμοσμένα αποθετήρια APT</dt>
     <dd>Συνηθίζεται να προσδιορίζει κανείς μερικά επιπλέον κλειδιά όταν ρυθμίζει προσαρμοσμένα αποθετήρια APT, χρησιμοποιώντας παραμέτρους όπως
       <tt>apt-setup/local0/key</tt>. Εξαιτίας αλλαγών στον τρόπο που δουλεύει 
       το <tt>apt-key</tt>, η προσπάθεια καθορισμού μιας τέτοιας ρύθμισης θα είχε σαν αποτέλεσμα αποτυχίες με τον εγκαταστάτη της έκδοσης Buster
       (<a href="https://bugs.debian.org/851774">851774</a>).
     <br />
     <b>Κατάσταση:</b>
       Η <a href="https://bugs.debian.org/851774#68">προτεινόμενη διόρθωση
       (patch)</a> δεν εξετάστηκε έγκαιρα για την αρχική κυκλοφορίας της έκδοσης Buster, αλλά αναμένεται να τεσταριστεί στη διάρκεια του κύκλου ανάπτυξης για την έκδοση Bullseye και να γίνει τελικά backported σε μια από τις σημειακές εκδόσεις της Buster.</dd>

<!-- things should be better starting with Jessie Beta 2...
	<dt>GNU/kFreeBSD support</dt>

	<dd>GNU/kFreeBSD installation images suffer from various
	important bugs
	(<a href="https://bugs.debian.org/757985"><a href="https://bugs.debian.org/757985">#757985</a></a>,
	<a href="https://bugs.debian.org/757986"><a href="https://bugs.debian.org/757986">#757986</a></a>,
	<a href="https://bugs.debian.org/757987"><a href="https://bugs.debian.org/757987">#757987</a></a>,
	<a href="https://bugs.debian.org/757988"><a href="https://bugs.debian.org/757988">#757988</a></a>). Porters
	could surely use some helping hands to bring the installer back
	into shape!</dd>
-->

<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce...
	<dt>Accessibility of the installed system</dt>
	<dd>Even if accessibility technologies are used during the
	installation process, they might not be automatically enabled
	within the installed system.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>Desktop installations on i386 do not work using CD#1 alone</dt>
	<dd>Due to space constraints on the first CD, not all of the expected GNOME desktop
	packages fit on CD#1. For a successful installation, use extra package sources (e.g.
	a second CD or a network mirror) or use a DVD instead.
	<br />
	<b>Κατάσταση:</b> It is unlikely more efforts can be made to fit more packages on CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Potential issues with UEFI booting on amd64</dt>
	<dd>There have been some reports of issues booting the Debian Installer in UEFI mode
	on amd64 systems. Some systems apparently do not boot reliably using <code>grub-efi</code>, and some
	others show graphics corruption problems when displaying the initial installation splash
	screen.
	<br />
	If you encounter either of these issues, please file a bug report and give as much detail
	as possible, both about the symptoms and your hardware - this should assist the team to fix
	these bugs. As a workaround for now, try switching off UEFI and installing using <q>Legacy
	BIOS</q> or <q>Fallback mode</q> instead.
	<br />
	<b>Κατάσταση:</b> More bug fixes might appear in the various Wheezy point releases.
	</dd>
-->

<!-- ditto...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
