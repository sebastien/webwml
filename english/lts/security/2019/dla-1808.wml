<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in SoX, the Swiss army knife of sound processing
programs, that could lead to denial of service via application crash or
potentially to arbitrary code execution by processing maliciously crafted
input files.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
14.4.1-5+deb8u4.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1808.data"
# $Id: $
