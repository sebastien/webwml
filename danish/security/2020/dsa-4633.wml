#use wml::debian::translation-check translation="d3f29d8015c29a9da9f70c50fcc3cb13a49a95c7" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i cURL, et URL-overførselsbibliotek.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5436">CVE-2019-5436</a>

    <p>Et heapbufferoverløb i TFTP-modtagelseskoden blev opdaget, hvilket kunne 
    muliggøre lammelsesangreb eller udførelse af vilkårlig kode.  Det påvirker 
    kun den gamle stabile distribution (stretch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5481">CVE-2019-5481</a>

    <p>Thomas Vegas opdagede en dobbelt frigivelse i FTP-KRB-koden, udløst af en 
    ondsindet server, som sender en meget stor datablok.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

    <p>Thomas Vegas opdagede et heapbufferoverløb, der kunne udløses når en 
    lille ikke-standard-TFTP-blokstørrelse blev anvendt.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 7.52.1-5+deb9u10.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 7.64.0-4+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine curl-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende curl, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4633.data"
