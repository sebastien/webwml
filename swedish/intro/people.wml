#use wml::debian::template title="Personer: vilka är vi, och vad vi gör"
#use wml::debian::translation-check translation="ac8343f61b83fe988dbcb035d77af5bf09ba0709"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<h2>Utvecklare och bidragslämnare</h2>
<p>Debian är skapat av nästan tusen aktiva
utvecklare som är utspridda
<a href="$(DEVEL)/developers.loc">över hela världen</a> och är volontärer 
på fritiden. 
Få av dessa utvecklarna har faktiskt träffats i verkligheten.
Kommunikation sköts primärt genom e-mail (sändlistor på
lists.debian.org) och IRC (#debian kanalen på irc.debian.org).
</p>

<p>Den fullständiga listan över officiella Debianmedlemmar kan hittas på
<a href="https://nm.debian.org/members">nm.debian.org</a>, där medlemskap hanteras.
En längre lista på Debian-bidragsgivare kan hittas på
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>Debianprojektet har en noggrant <a href="organization">organiserad
struktur</a>. För mer information om hur Debian ser ut från insidan,
så är du välkommen att surfa in på <a href="$(DEVEL)/">Debians utvecklarhörna</a>.</p>

<h3><a name="history">Hur det hela startade?</a></h3>

<p>Debian startades i Augusti 1993 av Ian Murdock, som en ny distribution
som skulle göras öppet tillgängligt, i samma anda som Linux och GNU. Debian var menat
att vara ömsesidigt och samvetsgrant ihopsatt, och att upprätthållas och
stödjas med liknande vård. De började som en liten, tätt sammansatt grupp av
Fri programvaruhackare, och växte gradvis till att bli en stor, välorganiserad
gemenskap av utvecklare och användare. Se
<a href="$(DOC)/manuals/project-history/">den detaljerade historiken</a>.

<p>Eftersom många har frågat, Debian uttalas /&#712;de.bi.&#601;n/. Det
kommer från namnet på skaparen av Debian, Ian Murdock, och hans fru,
Debra.
  
<h2>Individer och organisationer som stödjer Debian</h2>

<p>Många andra individer och organisationer är en del av Debiangemenskapen:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting och hårdvarusponsorer</a></li>
  <li><a href="../mirror/sponsors">Spegelsponsorer</a></li>
  <li><a href="../partners/">Utveckling och tjänstepartners</a></li>
  <li><a href="../consultants">Konsulter</a></li>
  <li><a href="../CD/vendors">Leverantörer av Debians installationsmedia</a></li>
  <li><a href="../distrib/pre-installed">Datorleverantörer som förinstallerar Debian</a></li>
  <li><a href="../events/merchandise">Debianprylar att köpa</a></li>
</ul>

<h2><a name="users">Vem använder Debian?</a></h2>

<p>Även om ingen exakt statistik finns tillgänglig (eftersom Debian inte
kräver att användare registrerar sig), så finns det rätt så starka bevis på att Debian används av
ett brett spektrum av organisationer, stora och små, såväl som
många tusen individer. Se vår <a href="../users/">Vem 
använder Debian?</a>-sida för en lista på högt profilerade organisationer som
har skickat in korta beskrivningar på hur och varför de använder Debian.
