#use wml::debian::translation-check translation="6704d39e66b84695501ff4999cb8a8971e14316b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Ampache, un système de
gestion de fichiers audio basé sur le web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12385">CVE-2019-12385</a>

<p>Le moteur de recherche est sujet à une injection SQL, aussi n’importe quel
utilisateur pouvant réaliser des recherches lib/class/search.class.php (même les
utilisateurs invités) peuvent copier n’importe quelles données contenues dans la
base de données (sessions, mots de passe chiffrés, etc.). Cela pourrait
conduire à une compromission totale des comptes administrateur lorsque cela est
combiné avec l’algorithme de génération de mot de passe faible utilisé dans la
fonction lostpassword.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12386">CVE-2019-12386</a>

<p>Un script intersite (XSS) stocké existe dans la fonctionnalité LocalPlay
<q>ajout d’une instance de fonction</q> de localplay.php . Le code injecté est
réfléchi dans le menu d’instances. Cette vulnérabilité peut être mal utilisée
pour forcer un administrateur à créer un nouvel utilisateur privilégié dont les
accréditations sont connues par l’attaquant.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.6-rzb2752+dfsg-5+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ampache.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1988.data"
# $Id: $
