#use wml::debian::template title="Sites-espelho mundiais do Debian" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f"

<p>O Debian é distribuído (<em>espelhado</em>) em centenas de servidores na
internet. Utilizando um servidor próximo provavelmente irá acelerar o seu
download e também reduzir a carga em nossos servidores centrais e na internet
como um todo.</p>

<p class="centerblock">
  Os espelhos do Debian existem em muitos países, e para alguns deles nós
  adicionamos um alias <code>ftp.&lt;country&gt;.debian.org</code>. Este alias
  geralmente aponta para um espelho que sincroniza regularmente e rapidamente,
  e carrega todas as arquiteturas do Debian. O repositório Debian está sempre
  disponível via <code>HTTP</code> localizado no <code>/debian</code> no
  servidor. 
</p>

<p class="centerblock">
  Outros <strong>sites de espelhos</strong> podem ter restrições sobre o que
  espelham (devido a restrições de espaço). Só porque um site não é o
  <code>ftp.&lt;country&gt;.debian.org</code> do país não significa
  necessariamente que seja mais lento ou menos atualizado do que o espelho
  <code>ftp.&lt;country&gt;.debian.org</code>.
  De fato, um espelho que carrega sua arquitetura e está mais próximo de
  você como usuário e, portanto, mais rápido, é quase sempre preferível a
  outros espelhos que estão mais distantes. 
</p>

<p>Utilize o site mais perto de você para baixar o mais rápido possível,
seja ele um alias do país ou não.
O programa
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> pode ser usado para determinar o site com menor
latência; utilize um programa de download como o
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> ou
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> para determinar o site com maior taxa de transferência.
Note que a proximidade geográfica geralmente não é o fator mais importante para
determinar qual máquina lhe servirá melhor.</p>

<p>
Se o seu sistema se move muito, você poderá ser melhor servido por um
"espelho" que é apoiado por uma
<abbr title="Content Delivery Network">CDN</abbr> global.
O projeto Debian mantém o domínio
<code>deb.debian.org</code> para este propósito e você pode usá-lo no seu
apt sources.list &mdash; consulte
<a href="http://deb.debian.org/">a página de serviços para detalhes</a>.

<p>A cópia oficial da seguinte lista pode sempre ser encontrada em:
<url "https://www.debian.org/mirror/list">.
Qualquer coisa a mais que você queira saber sobre espelhos do Debian, acesse:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Alias de espelho do Debian por país </h2>

<table border="0" class="center">
<tr>
  <th>País</th>
  <th>Site</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Lista de espelhos do repositório do Debian</h2>

<table border="0" class="center">
<tr>
  <th>Nome do host</th>
  <th>HTTP</th>
  <th>Arquiteturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
