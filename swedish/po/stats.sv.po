# translation of stats.po to Swedish
msgid ""
msgstr ""
"Project-Id-Version: stats\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik över översättningar på Debians webbplats"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Det finns %d sidor att översätta."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Det finns %d bytes att översätta."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Det finns %d strängar att översätta."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Felaktig översättningsversion"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Den här översättningen är föråldrad"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Originalet är nyare än denna översättning"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Originalet finns inte längre"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "träffräknare N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "träffar"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Klicka för att hämta diffstat-data"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Skapad med <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Översättningssammanfattning för"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Ej översatt"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Föråldrade"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Översatt"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Uppdaterade"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Observera: listorna på den här sidan sorteras efter popularitet. Hovra över "
"sidnamnet för att se antal träffar."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Föråldrade översättningar"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Git-kommandorad:"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Logg"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Översättning"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Underhållare"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Översättare"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Generella sidor ej översatta"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Ej översatta generella sidor"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nyheter som inte översatts"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Oöversatta nyheter"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Konsult/användarsidor som inte översatts"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Ej översatta Konsult/användarsidor"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Internationella sidor som ej översatts"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Ej översatta internationella sidor"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Översatta sidor (uppdaterade)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Översatta mallar (PO-filer)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Statistik för PO-översättningar"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Luddig"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Ej översatt"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Totalt"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Totalt:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Översatta webbsidor"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Översättningsstatistik efter antal sidor"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Språk"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Översättningar"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Översatta webbsidor (efter storlek)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Översättningsstatistik efter sidstorlek"

#~ msgid "Unified diff"
#~ msgstr "Samlad diff"

#~ msgid "Colored diff"
#~ msgstr "Färglagd diff"

#~ msgid "Commit diff"
#~ msgstr "Commit diff"

#~ msgid "Created with"
#~ msgstr "Skapad med"
