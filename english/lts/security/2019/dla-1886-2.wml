<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The latest security update of openjdk-7 caused a regression when
applications relied on elliptic curve algorithms to establish SSL
connections. Several duplicate classes were removed from rt.jar by the
upstream developers of OpenJDK because they were also present in
sunec.jar. However Debian never shipped the SunEC security provider in
OpenJDK 7.</p>

<p>The issue was resolved by building sunec.jar and its corresponding
native library libsunec.so from source. In order to build these
libraries from source, an update of nss to version 2:3.26-1+debu8u6 is
required.</p>

<p>Updates for the amd64 architecture are already available, new packages
for i386, armel and armhf will be available within the next 24 hours.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7u231-2.6.19-1~deb8u2.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1886-2.data"
# $Id: $
