#use wml::debian::template title="Exemplo de auditoria automatizada: RATS"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="be191e77facf8c0d489cfd320232517e5233a3e2"

<p>O <a href="https://packages.debian.org/rats">RATS</a> é um escaneador de
propósito geral para detectar potenciais problemas de segurança em algumas
linguagens de programação.</p>

<h2>Executando o RATS</h2>

<p>Executar o RATS é tão simples quanto invocar o comando com um diretório para
processar. Cada um dos arquivos de código-fonte reconhecidos será processado.
O RATS entende diversas linguagens de programação, C, Perl, PHP e Python,
e tratará cada uma como uma fonte válida para examinar.</p>

<p>Existem diversas opções que podem ser passadas além do nome de diretório para
escanear, elas estão descritas nas páginas man.</p>
<p>As opções mais úteis são aquelas relativas à saída, tais como:</p>

<ul>
<li>--warning &lt;nível&gt; (Define o nível de falhas a serem reportadas)
<ul>
<li>1 inclui somente padrão e alta severidade.</li>
<li>2 inclui média severidade (padrão).</li>
<li>3 inclui baixa severidade de vulnerabilidades.</li>
</ul></li>
<li>--xml  (saída em XML)</li>
<li>--html (saída em HTML)</li>
</ul>

<p>Assumindo que temos o <a href="test.c.html">arquivo teste</a> localizado
no diretório atual, e sem outros arquivos-fonte, podemos invocar o
escaneador com o seguinte comando::</p>
<pre>
rats --warning 1 --html . &gt;output.html
</pre>
<p>Isto produzirá um arquivo HTML contendo os resultados da busca, que pode
ser lido em um navegador.</p>

<h2>Os resultados</h2>

<p>Executar o RATS contra nossa <a href="test.c.html">amostra de código</a>
produz a seguinte saída:</p>
<hr />
# Nota do tradutor: trecho não traduzido, pois trata-se de saída de programa

<div class="sampleblock">

<h3>Severity: High</h3>

<p>Issue: fixed size global buffer</p>

<p>
  Extra care should be taken to ensure that character arrays that are
  allocated on the stack are used safely.  They are prime targets for
  buffer overflow attacks.
</p>
<p>
File: <b>test.c</b><br>
Lines: 10 11 12
</p>

<h3>Severity: High</h3>

<p>Issue: strcpy</p>

<p>
    Check to be sure that argument 2 passed to this function call will not
    copy more data than can be handled, resulting in a buffer overflow.
  </p>
<p>
File: <b>test.c</b><br>
Lines: 18
</p>

<h3>Severity: High</h3>

<p>Issue: getenv</p>

<p>Environment variables are highly untrustable input. They may be of any length, and contain any data. Do not make any assumptions regarding content or length. If at all possible avoid using them, and if it is necessary, sanitize them and truncate them to a reasonable length.
  </p>
<p>
File: <b>test.c</b><br>
Lines: 22 24   </p>

<h3>Severity: High</h3>

<p>Issue: sprintf</p>

<p>
    Check to be sure that the format string passed as argument 2 to this
    function call does not come from an untrusted source that could have added
    formatting characters that the code is not prepared to handle.
    Additionally, the format string could contain `%s' without precision that
    could result in a buffer overflow.
</p>
<p>
File: <b>test.c</b><br>
Lines: 24   </p>

<h3>Severity: High</h3>
<p>Issue: popen</p>
<p>
    Argument 1 to this function call should be checked to ensure that it does
    not come from an untrusted source without first verifying that it contains
    nothing dangerous.
</p>
<p>
File: <b>test.c</b><br>
Lines: 33   </p>

<h3>Severity: High</h3>

<p> Issue: printf</p>

<p>
    Check to be sure that the non-constant format string passed as argument 1
    to this function call does not come from an untrusted source that could
    have added formatting characters that the code is not prepared to handle.
</p>
<p>
File: <b>test.c</b><br>
Lines: 42   </p>
<p>
Total lines analyzed: <b>49</b><br>
Total time <b>0.000288</b> seconds<br>
<b>170138</b> lines per second</p>
</div>
<hr />

<p>Esta saída é bem volumosa, apesar do código mesmo ser bem curto - isto mostra
uma das desvantagens do escaneamento automático, que é a maior parte do volume
da saída.</p>

<h2>Entendendo a saída</h2>

<p>A saída que foi produzida é basicamente uma descrição das funções que ele
encontrou, o número da linha em que a falha foi detectada e uma descrição do
problema (como nós usamos o nível "--warning" - aviso - para restringir a
saída para somente funções de nível "alto", nós reduzimos a saída um
pouco).</p>

<p>Cada um dos problemas que foram descobertos devem ser manualmente examinados
para ver se existe alguma coisa errada realmente ou se foi um falso
positivo (isto é, uma função que pode ser mal usada, mas está sendo usada
corretamente).</p>

<p>Neste caso, podemos ver que todas as vulnerabilidades em nosso código
foram avistadas, mas não fica exatamente claro sem revisar o código com
um editor e examinar as linhas.</p>

<p>Uma grande omissão é que a saída não inclui as linhas que são reportadas -
algo que o <a href="flawfinder">flawfinder</a> permite que você inclua.</p>
<hr />

<p><a href="..">Voltar para o projeto de auditoria</a> | <a href="index">Voltar para a página de amostra de auditoria</a></p>
