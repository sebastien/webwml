#use wml::debian::template title="Debian wereldwijde spiegelservers" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f"

<p>
Debian wordt gedistribueerd (<em>gespiegeld</em>) via honderden servers op het
internet.  Het gebruik van een nabije server zal waarschijnlijk uw download
sneller laten gaan en vermindert ook de belasting op onze centrale servers en
het internet in het algemeen.
</p>

<p class="centerblock">
Er bestaan Debian spiegelservers in verschillende landen en voor sommige
hebben we een alias van de vorm <code>ftp.&lt;land&gt;.debian.org</code>
toegevoegd. Deze alias verwijst naar een spiegelserver die regelmatig en snel
gesynchroniseerd wordt en alle Debian architecturen bevat. Op de server is het
archief debian steeds beschikbaar via <code>HTTP</code> op de locatie
<code>/debian</code>.
</p>

<p class="centerblock">
Voor andere <strong>spiegelserversites</strong> kunnen restricties gelden in
verband met wat zij spiegelen (door opslagruimtebeperkingen). Een site die niet
de <code>ftp.&lt;land&gt;.debian.org</code> van een land is, is niet per
definitie langzamer of minder bijgewerkt dan de
<code>ftp.&lt;land&gt;.debian.org</code> spiegelserver. Eigenlijk valt een
spiegelserver die uw architectuur bevat en die
zich dicht bij u bevindt en daardoor sneller is, bijna altijd te verkiezen boven
andere verder afgelegen spiegelservers.
</p>

<p>
Gebruik de dichtstbijzijnde server voor de snelst mogelijke downloads, of deze
nu een spiegelserver is met een landspecifieke alias of niet. U kunt het programma
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> gebruiken om de server met de kortste vertraging te
bepalen; gebruik een downloadprogramma zoals
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> of
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> om te bepalen welke server de hoogste doorvoersnelheid
heeft. Merk op dat de geografische nabijheid vaak niet de belangrijkste factor
is voor het bepalen van de beste machine.
</p>

<p>
Indien u vaak rondtrekt met uw computer, bent u wellicht best
gediend met een "spiegelserver" die ondersteund wordt door een globaal
<abbr title="Content Delivery Network">CDN</abbr> (Content Delivery Network - Netwerk
voor het distribueren van gegevens). Met dit doel onderhoudt Debian
<code>deb.debian.org</code> en u kunt dit zo gebruiken in het bestand
sources.list voor apt. &mdash; raadpleeg voor bijkomende informatie
<a href="http://deb.debian.org/">de website van de betrokken dienst</a>.
</p>

<p>
De gezaghebbende versie van de volgende lijst is altijd beschikbaar op:
<url "https://www.debian.org/mirror/list">.
Alles wat u altijd al had willen weten over Debian-spiegelservers:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Debian-spiegelservers met een landspecifieke alias</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Site</th>
  <th>Architecturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Lijst met spiegelservers van het Debian-archief</h2>

<table border="0" class="center">
<tr>
  <th>Computernaam</th>
  <th>HTTP</th>
  <th>Architecturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
