<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was discovered in coturn, a TURN and STUN server for VoIP. By
default coturn does not allow peers on the loopback addresses
(127.x.x.x and ::1). A remote attacker can bypass the protection via a
specially crafted request using a peer address of <q>0.0.0.0</q> and trick
coturn in relaying to the loopback interface. If listening on IPv6 the
loopback interface can also be reached by using either [::1] or [::] as
the address.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.5.0.5-1+deb9u3.</p>

<p>We recommend that you upgrade your coturn packages.</p>

<p>For the detailed security status of coturn please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/coturn">https://security-tracker.debian.org/tracker/coturn</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2522.data"
# $Id: $
