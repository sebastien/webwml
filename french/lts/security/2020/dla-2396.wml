#use wml::debian::translation-check translation="07f1f4542ae27ab8d0d08302c40939e8985b6ace" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans tigervnc, une mise en œuvre de
serveur et client d’informatique virtuelle en réseau (VNC). L’implémentation
de l’afficheur gérait incorrectement les exceptions de certificats TLS, stockant
les certificats en tant qu’autorités, ce qui signifiait que le propriétaire d’un
certificat pouvait usurper l’identité de n’importe quel serveur après qu’un
client ait ajouté une exception.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.7.0+dfsg-7+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tigervnc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tigervnc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tigervnc">https://security-tracker.debian.org/tracker/tigervnc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2396.data"
# $Id: $
