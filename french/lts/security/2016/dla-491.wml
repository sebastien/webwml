#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le projet PostgreSQL a publié une nouvelle version de la branche 9.1 de
PostgreSQL :</p>

<ul>

<li>Nettoyage de la file d'attente d'erreurs d'OpenSSL avant les appels
d'OpenSSL, plutôt qu'assumer qu'elle est déjà nettoyée ; et assurance
qu'elle est nettoyée après (Peter Geoghegan, Dave Vitek, Peter Eisentraut)</li>

</ul>

<p>Cette modification évite les problèmes quand il y a de multiples
connexions utilisant OpenSSL à l'intérieur d'un processus unique et que
tout le code impliqué ne suit pas les règles sur le moment où nettoyer la
file d'attente d'erreurs. Des échecs ont été signalés particulièrement
quand une application client utilise des connexions SSL dans libpq en même
temps que des connexions SSL utilisant des enveloppes PHP, Python ou Ruby
pour OpenSSL. Il est possible que des problèmes similaires se produisent
aussi dans le serveur, si un module d'extension établit une connexion SSL
sortante.</p>

<ul>

<li>Correction d'une erreur « failed to build any N-way joins » du
planificateur avec une jointure complète incluse dans la partie droite
d'une jointure à gauche (Tom Lane)</li>

<li>Correction d'un possible mauvais comportement des codes de format TH,
th, et Y,YYY dans to_timestamp() (Tom Lane)</li>

</ul>

<p>Cela pourrait avancer la fin de la chaîne d'entrée, faisant que les
codes de format suivants lisent des données inutilisables.</p>

<ul>

<li>Correction du vidage des règles et des vues dans lesquelles l'argument
de table d'une construction d'opérateur « Value » « ANY (array) » est une
sous-requête (sub-SELECT) (Tom Lane)</li>

<li>Utilisation par pg_regress d'un délai de démarrage venant de la
variable d'environnement PGCTLTIMEOUT, si elle est configurée (Tom Lane)</li>

</ul>

<p>Cela est fait pour assurer la cohérence avec un comportement ajouté
récemment à pg_ctl ; il facilite les tests automatisés sur les machines
lentes.</p>

<ul>

<li>Correction de pg_upgrade pour restaurer correctement l'appartenance
d'extension pour les familles d'opérateur contenant une seule classe
d'opérateur (Tom Lane)</li>

</ul>

<p>Dans de tels cas, la famille de l'opérateur était restaurée dans la
nouvelle base de données, mais elle n'était plus marquée comme faisant
partie d'extension. Cela n'a pas d'effets néfastes immédiats, mais
pourrait faire que les exécutions ultérieures de pg_dump émettent une
sortie qui pourrait provoquer des erreurs (bénignes) lors de la
restauration.</p>

<ul>

<li>Renommage de la fonction interne strtoi() en strtoint() pour éviter un
conflit avec une fonction de la bibliothèque NetBSD (Thomas Munro)</li>

<li>Utilisation de l'indicateur FORMAT_MESSAGE_IGNORE_INSERTS si
nécessaire. Il n'y a pas de bogue actif connu, mais il semble que c'est une
bonne idée d'être prudent.</li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 9.1.22-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-491.data"
# $Id: $
