<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Yunus Çadırcı found an issue in the SUBSCRIBE method of UPnP, a
network protocol for devices to automatically discover and communicate
with each other. Insufficient checks on this method allowed attackers
to use vulnerable UPnP services for DoS attacks or possibly to bypass
firewalls.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.0.1-1+deb9u1.</p>

<p>We recommend that you upgrade your gupnp packages.</p>

<p>For the detailed security status of gupnp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gupnp">https://security-tracker.debian.org/tracker/gupnp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2315.data"
# $Id: $
