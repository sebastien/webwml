<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2017b. Notable
changes are:

<ul>
<li>Northern Cyprus resumed EU rules starting 2017-10-29.</li>
<li>Namibia will switch from +01 with DST to +02 all year, affecting
   UT offsets starting 2018-04-01.</li>
<li>Sudan will switch from +03 to +02 on 2017-11-01.</li>
<li>Tonga will not observe DST on 2017-11-05.</li>
<li>Turks &amp; Caicos will switch from -04 all year to -05 with US DST,
   affecting UT offset starting 2018-11-04.</li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2017c-0+deb7u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1155.data"
# $Id: $
